use V_K_module3
go
create view view_sweet
AS
SELECT 
ROW_NUMBER() OVER (ORDER BY sweet.title) AS id,
title,
volume,
price
FROM sweet

CREATE VIEW Sweet_info
AS SELECT sweet_id, inserted_date FROM cafee where YEAR (inserted_date) = 2018
GO

select*FROM Sweet_info
GO

CREATE VIEW view_wseet_orders
AS SELECT sweet_id from orders WHERE amount >5
go