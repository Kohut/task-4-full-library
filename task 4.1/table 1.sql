create database V_K_module3
go

create schema md_3
go

use V_K_module3
create table [md_3].Clients
( id int primary key identity (0,1),
f_name varchar (30) not null,
l_name varchar (30),
age int, 
client_weight int,
birth_date datetime,
registr_date datetime,
adress_index int,
gemder varchar (1),
favorite_cafee_id int,
favorite_drink_id int,
inserted_date datetime default getdate(),
updated_date datetime
constraint UniqueName UNIQUE(f_name, l_name)
)
GO
