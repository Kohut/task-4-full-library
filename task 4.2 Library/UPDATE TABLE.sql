use V_K_Kohut_library
GO

UPDATE Authors SET [URL] = 'www.harrypotter.com' where Author_id=1
UPDATE Authors SET [URL] = DEFAULT WHERE Author_id>18

UPDATE Books SET Price= Price*2  WHERE Price <= 4

UPDATE Books SET Price>=Price/2 WHERE Price>8

UPDATE Publishers SET [URL] = DEFAULT WHERE Publisher_id IN (4,6,3,1,8)

UPDATE Publishers SET [URL] = 'NO INFO' WHERE Publisher_id IN (5,10,13,18)

UPDATE BooksAuthors SET {Seq_No = DEFAULT WHERE (BooksAuthors_id %2)<>0

go