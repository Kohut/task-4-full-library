USE V_K_Kohut_library
go

CREATE TRIGGER [TR_AUTHOR_LOG] ON Authors_log
AFTER DELETE 

AS
IF EXISTS (select *from [Authors_Log])
BEGIN
PRINT ' DELETE RESTRICKT, CHECK YOU PERMISSION'
ROLLBACK TRANSACTION
END
GO

delete TOP 3 FROM BooksAuthors

DELETE Authors FROM Authors LEFT JOIN BooksAuthors on Authors.Author_id = BooksAuthors.Author_id WHERE BooksAuthors_id is null
DELETE Books FROM Books LEFT JOIN BooksAuthors ON Books.ISBN = BooksAuthors.ISBN WHERE BooksAuthors.ISBN IS NULL
DELETE Publishers FROM Publishers LEFT JOIN Books ON Publishers.Publisher_id = Books.Publisher_id WHERE Books.Publisher_id IS NULL
GO

DISABLE TRIGGER TR_AUTHOR_lOG ON Authors_log
go

DELETE TOP (3) FROM Authors_Log
enable trigger TR_AUTHOR_LOG on Authors_log
